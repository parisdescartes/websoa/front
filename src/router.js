import Vue from "vue";
import Router from "vue-router";
import Login from "./components/Login";
import FormInscription from "./components/FormInscription";
import DocumentList from "./components/DocumentList";
import FileUpload from "./components/FileUpload";
import Document from "./components/Document";
Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/signin",
            component: Login
        },
        {
            path: "/signup",
            component: FormInscription
        },
        {
            path: "/fileslist",
            component: DocumentList
        },
        {
            path: "/uploadfile",
            component: FileUpload
        },
        {
            path: "/aboutus",
        },
        {
            path: "/document",
            component : Document
        },
    ]
});
