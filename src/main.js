import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueSession from "vue-session";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.config.productionTip = false;
Vue.use(VueSession, {persist:true});
Vue.use(Vuetify);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
